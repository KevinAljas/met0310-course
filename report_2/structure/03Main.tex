\section{Introduction}
The goal of this paper is to setup an existing ROS workspace to utilize the existing Turtlebot3 simulations. By the end of this paper, we will have a working simulation of a Turtlebot3 Waffle robot, which can automatically navigate to set destinations in a pre-mapped world.

\section{Setting up the workspace}
\subsection{Including the Turtlebot3 git repositories}
The Turtlebot3 simulations are hosted on a git repository that anyone can use. Since our existing workspace is also a git repository, it doesn't make sense to clone the Turtlebot3 repositories into our repository. \cite{submodules}

Instead, there is an alternative way, called git submodules. Submodules are a way of including external git repositories inside other repositories. This has the advantage of not messing with commits and saving on repository space. \cite{practice}

\begin{lstlisting}
    git submodule add https://github.com/ROBOTIS-GIT/turtlebot3.git
    git submodule add https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
    git submodule add https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
\end{lstlisting}

Before executing those commands, we need to make sure we include them in the correct directory. Since these git repositories contain ROS packages, we need to add them into our workspace "src" directory.

\subsection{Rebuilding the workspace}
To make sure that the ROS packages were properly installed and compiled, we need to do a clean rebuild of our workspace.

In our workspace root folder, execute:
\begin{lstlisting}
    catkin_make clean -j6
    catkin_make -j6
\end{lstlisting}

The -j6 flag is optional and specifies the amount of threads to use in the compiling process. On stronger machines, it's generally set as -j16, however the author is writing this document on an espresso maker. The -j flag can be left out if the user is unsure about its purpose.

Make sure that the compilation output has references to Turtlebot3 to check that the git submodules are building properly.

After re-compiling the workspace, source the setup script again with
\begin{lstlisting}
    source devel/setup.sh
\end{lstlisting}

\subsection{Extra packages needed for Turtlebot simulations}
During the course of this document, the author found that multiple packages needed to be installed manually, here is a list of some of them:
\begin{lstlisting}
    sudo apt install ros-noetic-amcl \
    ros-noetic-move-base \
    ros-noetic-dwa-local-planner \
    ros-noetic map-server \
\end{lstlisting}

People following this document may find more packages that need to be installed manually, these can very easily be found in the error messages of different commands. Every ROS Noetic package starts with ros-noetic in the Ubuntu repositories. Using apt search is your friend here.

\subsection{System variables}
The Turtlebot3 simulations support using multiple robots. Before starting any of the relevant Turtlebot3 simualtions, we need to specifiy which robot to use. In this document, we will be using the Waffle robot. \cite{practice}

In order to specify to the ROS launch files, which robot we want to use, we use a system variable with a name of TURTLEBOT3\textunderscore MODEL.

To specify that we want to use the Waffle robot, we type
\begin{lstlisting}
    export TURTLEBOT3_MODEL=waffle
\end{lstlisting}

This needs to be done in every terminal using the relevant Turtlebot3 simulations, which is really annoying. An elegant solution for this is adding the export command into the users .bashrc file, as such:
\begin{lstlisting}
    echo "export TURTLEBOT3_MODEL=waffle" >> ~/.bashrc
\end{lstlisting}

After executing the echo command, all terminals that were opened beforehand need to either re-source the .bashrc file or be restarted. To check that the export command is working automatically, as expected, open a new terminal and write
\begin{lstlisting}
    echo ${TURTLEBOT3_MODEL}
    # Prints out: waffle
\end{lstlisting}

\section{Testing the Turtlebot3 simulations}
Once the git submodules have been added and compiled, extra packages installed and system variables set, we can test the Turtlebot3 simulations.

\subsection{Gazebo overview}
To start the main simulation, type:
\begin{lstlisting}
    roslaunch turtlebot3_gazebo turtlebot3_world.launch
\end{lstlisting}

This command may take some time, but eventually a Gazebo window should open up and you should see a turtle shaped enclosed area with a Turtlebot3 model waffle inside of it.

Gazebo is a simulation environment to test physical designs and improve on them in software. \cite{gazebo}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/gazebo.png}
    \caption{\label{fig:gazebo}Overview of the Gazebo program after launching turtlebot3\textunderscore world.}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{images/waffle.png}
    \caption{\label{fig:waffle}Overview of the Waffle robot selected by the system variable.}
\end{figure}

\subsection{Controlling the Turtlebot}
In previous chapters we controlled our R2D2 robot using an existing teleoperations package, which sent movement commands with the "cmd\textunderscore vel" topic. This topic was subscribed to by the R2D2 controller which made the robot move in the RViz environment.

The turtlebots use the same "cmd\textunderscore topic to send movement commands. However the Turtlebot3 repositories include a more sophisticated teleoperations program to control the robot a bit more precisely. 

The turtlebot3 teleoperations command can be launched with the following command:
\begin{lstlisting}
    roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
\end{lstlisting}

After a full launch, the robot can be controlled with WASDX keys.
W and X increase or decrease the robots linear speed.
A and D increase or decrease the robots angular speed.
S forces the robot to stop all movement.

The main difference between this new teleoperations program and the old one is that the new one doesn't require the user to keep pressing the movement buttons to make the robot move. Once the desired speeds have been set, the robot will keep moving with those speeds until it is stopped or the speed is changed. \cite{practice}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/moving.png}
    \caption{\label{fig:moving}Waffle-bot moving with a linear and angular speed.}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/teleop.png}
    \caption{\label{fig:teleop}Terminal snippet of the teleop launch, WASDX keys are used for movement.}
\end{figure}

\subsection{Turtlebot sensors}
The Turtlebot3 is equipped with a LIDAR and camera which lets the turtlebot see the external world. There is an additional ROS launch file to see the environment through the eyes of the LIDAR.

To visualize the sensory data, we can launch the Rviz program with the following command:
\begin{lstlisting}
    roslaunch turtlebot3_gazebo turtlebot3_gazebo_rviz.launch
\end{lstlisting}

Running this command with the previous Gazebo and teleoperations commands, a new Rviz window opens up, which lets us see the LIDAR collision points. \cite{practice}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/lidar.png}
    \caption{\label{fig:lidar}Overiew of the Rviz 2D view, red dots are where the LIDAR sees the wall.}
\end{figure}

In figure 5, all the red dots indicate where the LIDAR detects a collision, or in our case, where the turtlebot sees a wall.
We can tell from the red dot positions that the turtlebot seems to be between 4 cylindrical objects, and there is a far away wall to the south of it.

Checking the layout of the world from figure 1, we can resonably assume that the turtlebot moved north past the first pillars from the starting position.

\section{Mapping an environment}
By the end of this document, we want the turtlebot to be able to drive automatically to a target point specified by the user. To do this, the turtlebot needs to know the full layout of the world. As mentioned before, the turtlebot is equipped with a LIDAR, which is perfect for this.

\subsection{Starting the gmapping algorithm}
Let's re-launch the turtlebot world-file and the teleoperations communicator. In addition, lets start a new launch script, which will use the LIDAR and the "gmapping" algorithm to map the environment. \cite{practice}

\begin{lstlisting}
    roslaunch turtlebot3_gazebo turtlebot3_world.launch
    roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
    roslaunch turtlebot3_slam turtlebot3_slam.launch \ slam_methods:=gmapping
\end{lstlisting}

After a successful launch, to windows will open up. The first one is the Gazebo world, which we are already familiar with, the other is a different Rviz program, which in addition to the LIDAR output, will keep track of the position of the robot and all the obstacles the robot has detected.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/unmapped.png}
    \caption{\label{fig:unmapped}Overiew of the Rviz 2D view, waffle-bot is in the default position and already sees some obstacles.}
\end{figure}

\subsection{Driving the waffle-bot around}
Using the teleoperations launch, we can see the waffle-bot move in both windows. Right now we are only concerned with the Rviz window. Our goal is to drive around the turtle world and map all the external walls and the internal cylinders. \cite{practice}

As you drive around, you will notice differently colored pixels.
Gray pixels indicate empty space that the robot has found and can move to.
Black pixels indicate an obstacle, such as a wall or a pillar.
Green pixels indicate the current LIDAR datapoints.
Transparent or "Valve HUD colored" background indicates a place the turtlebot has not scanned.

Driving the robot around the external walls and between the 9 cylinders, it's important to collect enough datapoints. To make sure that you have a full and correct mapping, make sure that the external walls are fully traced with black pixels, with no gray pixels between them. Similarly, make sure that the internal cylinders resemble circles and that they do not have any gaps.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/mapped.png}
    \caption{\label{fig:mapped}Example of how a fully mapped world may look like, notice the lack of caps in the obstacles and walls.}
\end{figure}

\subsection{Saving the mapped world}
Once the world has been fully mapped, it can be saved with the following command:
\begin{lstlisting}
    rosrun map_server map_saver -f mymap
\end{lstlisting}

In the context of this document, the saved map files were then manually moved into a new directory, inside the ROS workspace src/ directory, called "map".

After the map is fully saved, we can shut down all the terminals. \cite{practice}

\section{Navigation \& pathfinding}
After successfully mapping the world-file, the turtlebot now has all the information necessary to navigate the world.

This time, lets restart the Turtlebot3 world-file but also the navigation node, specifiying the path to the map file we just created. Notice that the location of the map file is machine specific and will need to be edited accordingly.

\begin{lstlisting}
    roslaunch turtlebot3_gazebo turtlebot3_world.launch
    roslaunch turtlebot3_navigation turtlebot3_navigation.launch \
        map_file:=${HOME}/Projects/met0310-course/src/map/mymap.yaml
\end{lstlisting}

This will open up the familiary Gazebo and Rviz views.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/init_path.png}
    \caption{\label{fig:init_path}Initial view of the mapping Rviz.}
\end{figure}

You may notice that the mapped world and current sensor data do not match. This can be fixed with the "2D Pose Estimate" tool.

To use it, select "2D Pose Estimate" on the top bar of the Rviz window. Then place your cursor to the place the turtlebot actually is and click and hold, while holding down, point the arrow in the direction the wafflebot is looking to. If done right, the pre-mapped world and sensorics should match. It may take a few fine-tuning adjustments to get the alignment just right.

This is what the Rviz window should roughly look like when the sensor data and mapped world are aligned:
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/align_path.png}
    \caption{\label{fig:align_path}Aligned view of the mapping Rviz.}
\end{figure}

Now we are ready to move the turtlebot. This can be done with the "2D Nav Goal" tool. Simply click "2D Nav Goal" on the top bar and then click to where you want the robot to navigate to. It will automatically find a path to the target destination and start driving there. On the way it may make some recalculations and choose an alternative route.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/pathfinding.png}
    \caption{\label{fig:pathfinding}Rviz view of the turtlebot in motion, following the indicated path.}
\end{figure}

\subsection{RQT graph analysis}
Lets take a closer look at the RQT graph of the nodes from the last chapter.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/rqt_graph.png}
    \caption{\label{fig:rqt_graph}RQT graph overviewing the ROS nodes and topics of the previous chapter.}
\end{figure}

The RQT graph is much more complex than the previous simulations we have worked on, but getting an oversimplified idea, we can see that the "move\textunderscore base" node is responsible for all the pathfinding calculations, it gets the map information from the "map\textunderscore server" node and calculates all the cost maps and necessary paths. The map\textunderscore server also gets data from the "gazebo" node with all the turtlebot sensor data to keep track of the current position of the robot. In addition to getting sensor data from the "gazebo" node, the "move\textunderscore base" also sends movement commands via the familiar "cmd\textunderscore vel" topic to order the gazebo to move the robot.

\subsection{2D Pose Estimate}
The 2D Pose Estimate command allows the user to initialize the localization system used by the navigation stack by setting the pose of the robot in the world manually. \cite{2d_comms}

It uses the topic "initialpose" to communicate and we can see from the RQT graph that the "acml" localization node is subscribed to it. Information from the "initialpose" topic is then used to edit the "acml" and "particlecloud" nodes.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/initialpose.png}
    \caption{\label{fig:initialpose}RQT graph overviewing the 2D Pose Estimate nodes and topics.}
\end{figure}

\subsection{2D Nav Goal}
The 2D Nav Goal allows the user t o send a goal to the navigation by setting a desired pose for the robot to achieve. \cite{2d_comms}

It uses the "move\textunderscore base\textunderscore simple/goal" topic to send out goals to the "move\textunderscore base" node, which calculates the optimal path to the target point provided by the "goal" topic.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/nav_goal.png}
    \caption{\label{fig:nav_goal}RQT graph overviewing the 2D Nav Goal nodes and topics.}
\end{figure}