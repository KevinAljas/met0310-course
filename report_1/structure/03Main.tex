\section{Introduction}
The goal of this chapter is to create a model of the infamous R2D2 from Star Wars. The model will be designed with URDF, which stands for "Unified Robotics Describtion Format".

URDF is an XML format for describing a robot model. It has features for defining the robot shape and the joints between shapes. In addition it can define hitboxes and the material properties of the robot. \cite{ros_urdf}

\section{Building the URDF model}
\subsection{Creating a first shape}
As mentioned before, URDF uses the XML format. Before we can start defining shapes, we need to create the starndard XML header and define the robot name.

\begin{lstlisting}
<?xml version="1.0"?>
<robot name="r2d2">
</robot>
\end{lstlisting}

This creates an empty robot with a name of "r2d2". Currently our R2D2 is totally empty, lets add some shapes.
R2D2 is well known for their cylindrical body, so lets create a cylinder.\cite{ros_urdf}

First, we need to define a link, which for our purposes, acts as a parent container for everything related to a specific part of the robot.
A link is defined with the aptly named link tag and it needs a name as an argument.
Inside the link we can store a visual section, which holds everything related to graphics. Inside the visual section, we store the geometry section, which in turn holds everything related to shapes. Finally, inside the geometry section, we can define the shape as an XML header. The cylinder takes a length and radius as arguments.
We want this base to be a cylinder and we'll give it an arbitrary length 
of 0.6 meters and radius of 0.2 meters.

Our code should now look like this:
\begin{lstlisting}
<?xml version="1.0"?>
<robot name="r2d2">
    <link name="base_link">
        <visual>
            <geometry>
                <cylinder length="0.6" radius="0.2"/>
            </geometry>
        </visual>
    </link>
</robot>
\end{lstlisting}

We can see our result by doing the usual ROS setup and launching the URDF example project. Make sure that the model name matches.
\begin{lstlisting}
source devel/setup.sh
roslaunch urdf_tutorial display.launch model:=r2d2.urdf
\end{lstlisting}

The roslaunch command launches RViz, which is a 3D rendering software. The end result should look like this:
\begin{figure}[h]
    \centering
    \includegraphics[width=0.3\textwidth]{images/first_shape.png}
    \caption{\label{fig:first_shape}The base link as defined in the URDF file.}
\end{figure}

\subsection{Spicing things up}
R2D2 is not just a red cylinder. We need to create more shapes and add color to our objects. \cite{r2d2} 

First of all, we need to be able to create a box of any shape.
Similar to the cylinder command we can use a box command to define, well, a box. It takes three arguments, which are the three side lengths.

For example, this code replaces our cylinder with a box:
\begin{lstlisting}
<?xml version="1.0"?>
<robot name="r2d2">
    <link name="base_link">
        <visual>
            <geometry>
                <box size="1 1 0.5"/>
            </geometry>
        </visual>
    </link>
</robot>
\end{lstlisting}

Our box looks like this:
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.3\textwidth]{images/box.png}
    \caption{\label{fig:box}Box with side lengths 1x1x0.5 meters.}
\end{figure}

\newpage

Next up, we want to add color to our objects. R2D2 is usually blue and white. 
We can add color to objects with the material property. Inside the material property we can set an rgba value for that material. RGB for red, green and blue and A for transparency.

The material is defined once inside the robot section and then is applied to each links visual section. This means you don't need to retype the RGBA values for every same-colored object.

For example, if we wanted to create a blue material and then add it to our existing cylinder, it would look like this:
\begin{lstlisting}
<?xml version="1.0"?>
<robot name="r2d2">
    <material name="blue">
        <color rgba="0 0 0.8 1"/>
    </material>

    <link name="base_link">
        <visual>
            <geometry>
                <cylinder length="0.6" radius="0.2"/>
            </geometry>
            <material name="blue"/>
        </visual>
    </link>
</robot>
\end{lstlisting}

The result would look like this:
\begin{figure}[h]
    \centering
    \includegraphics[width=0.3\textwidth]{images/blue_cylinder.png}
    \caption{\label{fig:blue_cylinder}A blue cylinder instead of the default red.}
\end{figure}

\newpage

\subsection{R2D2 model}
Now we have the basic building blocks for creating a simplified model of an R2D2. Defining some more geometry objects and colors we can create this:

\begin{figure}[h]
    \centering
    \includegraphics[width=0.3\textwidth]{images/r2d2_basic.png}
    \caption{\label{fig:r2d2_basic}Simplified URDF model of an R2D2.}
\end{figure}

The main body is defined with a cylinder and sphere, stacked ontop of eachother so that the sphere looks like a half-sphere. Each leg is made up of 2 boxes and 2 black cylinders for wheels. In addition, on top of the head, there is a box and cylinder emulating the lens or camera of the robot.

\subsection{Hitboxes and collisions}
Currently our R2D2 model is made up of etherial geometry blocks. For future actions, we want our robot to be able to interact with the world. \cite{ros_collision}

To achieve this, each of our link needs to have collision section. This section goes inside the link section and contains in itself a geometry section, which describes the hitbox.
Usually we want the collision hitbox to be a simplified version of our model, there is no need to model the exact detail in a complex shape when a well fitted box would achieve the same result with much better performance.
However, in our case, since our model is already almost entirely made up of simple geometric objects. This means that most of our collision geometry sections are identical to the visual geometry sections.

Creating a hitbox for our base link looks like this:
\begin{lstlisting}
    <?xml version="1.0"?>
    <robot name="r2d2">
        <material name="blue">
            <color rgba="0 0 0.8 1"/>
        </material>
    
        <link name="base_link">
            <visual>
                <geometry>
                    <cylinder length="0.6" radius="0.2"/>
                </geometry>
                <material name="blue"/>
            </visual>

            <collision>
                <geometry>
                    <cylinder length="0.6" radius="0.2"/>
                </geometry>
            </collision>

        </link>
    </robot>
\end{lstlisting}

This doesn't change anything visually so the effect isn't visible in our current RViz viewer.

\subsection{Mass and inertia}
Currently our robot doesn't have any mass. This is not a problem for our basic model but for advanced models and simulations, the model having mass is quite important.
Such physical properties are stored in the inertial section, which each link should have.
Inside the inertial section, we can store the mass and inertia values.
The mass section takes one argument, the mass in kilograms.
The inertia section is more complex, taking a 3x3 inertia matrix. The 3x3 matrix can be described by 6 values, since it is symmetrical. \cite{ros_collision}

While the ROS tutorial recommends against it, lets use a basic identity matrix for the interia value, since we aren't doing any precise simulations.
As for the mass, lets use 1kg for every object.

\begin{lstlisting}
    <inertial>
        <mass value="1" />
        <inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="1.0" iyz="0.0" izz="1.0" />
    </inertial>
\end{lstlisting}

This section would need to go to each link but before doing it manually, lets look at a better method.

\subsection{Xacro properties and macros}
As we start creating more complex robots, the amount of magical constants in our URDF file increases drastically. Should you wish to change one specific measurement, you would need to go through the entire file manually and change each occurance of this value and all the values that derive from it.

This is incredibly tedious, which is why Xacro comes in. Xacro allows the user to create macros in the URDF file and then reference these macros, instead of the value itself.
Xacro can be thought of as the pre-processor in C, which takes cleans up the code before passing it on to the compiler. \cite{ros_xacro}

With the addition of collisions to our URDF file, you might have noticed that we are, in essence, referencing each shape twice. This is an excellent place to use our Xacro macros.
Lets define a property, with a name and a value. Before launching the URDF file, all references to Xacro properties are found and replaced with the value they replace.

The syntax to create a property is:
\begin{lstlisting}
    <xacro:property name="width" value="0.2" />
    <xacro:property name="bodylen" value="0.6" />

    ... Base link definition ...
    <cylinder radius="${width}" length="${bodylen}"/>
    ... Rest of the code
\end{lstlisting}

This creates a variable called "width" and "bodylen", which hold the values 0.2 and 0.6 respectively. To reference these values, we type a dollar sign and a pair of curly brackets. The variable name goes inside the curly brackets.
In the example above, we used the Xacro properties to give a the measurements of the base link cylinder. In the future, when we want to change the cylinder size, we can just edit the property at the top of the file, instead of parsing through it manually.

You can also do basic math with the Xacro properties, for example:
\begin{lstlisting}
    <xacro:property name="width" value="0.2" />
    <xacro:property name="bodylen" value="0.6" />

    ... Base link definition ...
    <cylinder radius="${width + 1}" length="${bodylen / 2}"/>
    ... Rest of the code
\end{lstlisting}

This is especially useful for offseting objects, since offsets are usually related to the measurements of some objects. Using Xacro macros you can write and URDF file such that you can change any measurement and the model would still work.

We can also use Xacro macros to replace larger amounts of code. Let's look at our inertial and mass example. If you remember, we want our inertial value to be the same for every object. The inertial section doesn't differ that much between each link, so it does not make much sense to just copy-paste it everywhere. We can define an Xacro macro, which is a more powerful version of the Xacro property. Instead of holding a single value, it multiple lines of code. In addition it can take arguments. It acts very much like a function in typical programming languages.

We create a Xacro macro, it has a name of "default inertial" and takes as an parameter mass.
Every time we reference this macro, it gets replaced with the contents of the macro, such that the paramater mass is edited into it.
\begin{lstlisting}
// Defining the Xacro macro
<xacro:macro name="default_inertial" params="mass">
<inertial>
    <mass value="${mass}" />
    <inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="1.0" iyz="0.0" izz="1.0" />
</inertial>
</xacro:macro>

// Using the Xacro macro
<link name="base_link">
    <visual>
        ... Misq. code ...
    </visual>
    <collision>
        ... Misq. code ...
    </collision>
    <xacro:default_inertial mass="10"/>
</link>

// Gets converted into
<link name="base_link">
    <visual>
        ... Misq. code ...
    </visual>
    <collision>
        ... Misq. code ...
    </collision>
    <inertial>
        <mass value="10" />
        <inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="1.0" iyz="0.0" izz="1.0" />
    </inertial>
</link>
\end{lstlisting}

With these macros, we can clean up our code considerably and make it much easier to maintain and edit in the future.

\subsection{Joints}
Currently the shapes that make up our R2D2 are not really connected in any way. In addition to the visual shape and collision boxes, we need to describe how the objects are connected to each other. Some others need to be fixed together, so that they don't move relative to each other and stay together. Sometimes we want some object to have a few degrees of freedom so that it can rotate or translate relative to the parent object.

There are many different joint types. Joints are not defined inside the links but inside the main "robot" section. In general, a joint has a name, type, parent and child, and XYZ offsets and rotations. \cite{ros_joints}

The "fixed" joint is used to remove all degrees of freedom between two objects. If the parent object moves or rotates, the child object moves or rotates along. \cite{ros_joints}
\begin{lstlisting}
<joint name="tobox" type="fixed">
    <parent link="head"/>
    <child link="box"/>
    <origin xyz="${.707*width+0.04} 0 ${.707*width}"/>
</joint>
\end{lstlisting}

This is an example joint from the finished R2D2 model, which ties together the spherical head and the box-shaped box.
We define a joint name, it could be anything, in the example it's "tobox".
We don't want the "box" to be able to move by itself, so we use the "fixed" type of joint.
We want the "box" to be connected to the "head", so the "head" is the parent and "box" the child.
We also need to define the XYZ offset of the joint, so that the "box" rotates along properly with the "head".

\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{images/fixed_joint.png}
    \caption{\label{fig:fixed_joint}Fixed joint between the head and the box, notice how the box stays "fixed" to the sphere, even if the sphere is moved}
\end{figure}

The "continious" joint is used to allow the child object to rotate around the parent object.
Similarly to the fixed joint, we define a name, type, parent and child, and origin. 

\newpage
In addition, we need to define the rotation axis, as a XYZ vector. \cite{ros_joints}
\begin{lstlisting}
<joint name="head_swivel" type="continuous">
    <parent link="base_link"/>
    <child link="head"/>
    <axis xyz="0 0 1"/>
    <origin xyz="0 0 ${bodylen/2}"/>
</joint>
\end{lstlisting}

This is an example joint from the finished R2D2 model, which ties together the base cylinder and the spherical head. The head needs to be able to rotate around the Z axis.

\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{images/fixed_joint.png}
    \caption{\label{fig:continious_joint}Continious joint between the head and base cylinder. The head can rotate 360 degrees around the Z-axis.}
\end{figure}


The "prismatic" joint is used to allow the child object to move on a linear axis relative to the parent object.
Similarly to the fixed joint, we define a name, type, parent and child, and origin.

In addition, we need to define a "limit", which specifies the maximum speed, force and reach of the extension.
For our model, we do not care about the velocity and effort values and they will be left as 0.5 and 1000.0 respectively. \cite{ros_joints}

\begin{lstlisting}
<joint name="lens_extension" type="prismatic">
    <parent link="box"/>
    <child link="lens"/>
    <limit effort="1000.0" lower="-${lens_len / 2}" upper="0" velocity="0.5"/>
    <origin rpy="0 0 0" xyz="0 0 0"/>
</joint>
\end{lstlisting}

This is an example joint from the finished R2D2 model, which ties together the black lens and the box. The lens is allowed to "zoom" in and out of the box.
\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{images/prismatic_joint.png}
    \caption{\label{fig:prismatic_joint}Prismatic joint between the lens and the box. The picture shows the maximum and minimum position of the lens.}
\end{figure}

The final R2D2 model with colors, collisions, joints and Xacro macros can be found on the ROS wiki tutorials. \cite{ros_xacro}

\section{Moving the R2D2}

Now that we have fully defined our R2D2 model alongside with collisions, color and inertial properties, it's time to move our robot.
We'll be using the RViz 3D renderer for this along with a custom-made laucnh file.

\subsection{Movement}
To move the R2D2, we will be using the TurtleSim turtle teleop key script.

We can run this script with the rosrun command:
\begin{lstlisting}
// Make sure ROS environment is sourced
source devel/setup.sh

// Launch turtlesim
rosrun turtlesim turtle_teleop_key
\end{lstlisting}

\subsection{Creating the launch file}
To move the R2D2 in the RViz environment, we need to start a lot of nodes. Doing this manually in ten different terminals is very cumbersome. It is time to create our own launch file.

A launch file is a special text-file, that is used by the roslaunch command to easily start a large amount of nodes. It can do a lot of other things, that we will look at afterwards.

The launch file uses the XML format and everything is contained in the launch tag.
In the launch tag, we describe from top to bottom, what needs to be done.

The first thing we need to do, is parse our Xacro based URDF file into a normal URDF file.
Then we need to start two nodes, called "robot\textunderscore state\textunderscore publisher" and "joint\textunderscore state\textunderscore publisher".
Then we need to start the r2d2\textunderscore navigator node.
Then we need to start RViz, to actually see the movement.

After the launch file is launched, we start the "turtle\textunderscore teleop\textunderscore key" in a seperate terminal, so that we can send key-pressed to the "r2d2\textunderscore navigator".

The final launch file should look something like this, keeping in mind that the relative and absolute paths may differ: \cite{ros_launch}
\begin{lstlisting}
<launch>
    <param name="robot_description" command ="xacro --inorder '/home/jaanus/Projects/met0310-course/src/kevin_r2d2/urdf/r2d2.urdf.xacro'"/>

    <node name="robot_state_publisher" pkg="robot_state_publisher" type ="robot_state_publisher"/>
    <node name="joint_state_publisher" pkg="joint_state_publisher" type ="joint_state_publisher"/>
    
    <node name="r2d2_navigator"        pkg="r2d2_navigator"        type ="fake_r2d2_controller"/>

    <node name="rviz" pkg ="rviz" type ="rviz" args="-d /home/jaanus/Projects/met0310-course/src/kevin_r2d2/config/r2d2.rviz"/>
</launch>
\end{lstlisting}

\subsection{Redirecting the topics}
As mentioned before, the "turtle\textunderscore teleop\textunderscore key" uses a topic named "/turtle1/cmd\textunderscore vel" to send movement commands.
The "r2d2\textunderscore navigator" package however, is subscribed to a topic named "/cmd\textunderscore vel".

Because of the difference in topic names, the "turtle\textunderscore teleop\textunderscore key" and "r2d2\textunderscore navigator" nodes cannot communicate between eachother.
We need a way to redirect the "/turtle1/cmd\textunderscore vel" traffic to "/cmd\textunderscore vel/". 

This is where the "remap" command comes in. We can use "remap" to make it that when a node subscribes to topic X, it actually subscribes to topic Y.
Using "remap", we can make it that when "r2d2\textunderscore navigator" tries to subscribe to "/cmd\textunderscore vel", it actually subscribes to "/turtle1/cmd\textunderscore vel".

It's important that the remap command is called before the "r2d2\textunderscore navigator" node is activated, since the remap only applies to all subsequent nodes.
Let's add the following line to our launch script: \cite{ros_launch}

\begin{lstlisting}
... launch script ...

<remap from="/cmd_vel" to="/turtle1/cmd_vel"/>
<node name="r2d2_navigator" pkg="r2d2_navigator" type ="fake_r2d2_controller"/>

... launch script ...
\end{lstlisting}

To launch the script, write:
\begin{lstlisting}
    roslaunch package_name launchfile_name

    In the authors case:
    roslaunch kevin_r2d2 r2d2.launch
\end{lstlisting}

If everything is correct, the RViz program should start up and the R2D2 should be visible. The roslaunch command should not return any hard errors that are colored red. We still can't move the model because we need to start the turtlesim.

Before closing the RViz program, select "odom" as the Fixed Frame. This helps us see the model move because by default the camera is tied to the robot. After changing the fixed frame, save the RViz configuration with the same name that is specified in the launch file. In the current example, "r2d2.rviz". This way the "odom" is set as the fixed frame automatically the next time you launch the program.

\subsection{Finally moving the model}
Now that our launch file is functional, it's time to actually move the model.

First, start the launch file and then start the turtlesim script. There is no need to start the "roscore" because "roslaunch" does that automatically.
Normally this proceedure requires 2 terminals but it can be done with only one. \cite{ros_launch}

The author wrote a small bash script to start the whole setup with just one command.
\begin{lstlisting}
#! /bin/bash

source /home/jaanus/Projects/met0310-course/devel/setup.sh

roslaunch kevin_r2d2 r2d2.launch &
sleep 5

clear
rosrun turtlesim turtle_teleop_key
\end{lstlisting}

This script first makes sure that the ROS environment is sourced. Then it runs "roslaunch" in the background, indicated by the ampersand.
The script then waits 5 seconds for the launch to finish. Then the terminal is cleared and the turtlesim script started.

You can move the model by pressing arrow-keys in the turtlesim terminal. You should be able to see the model move in the RViz 3D view.

\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{images/movement.png}
    \caption{\label{fig:movement}The model has moved from the starting point "odom".}
\end{figure}